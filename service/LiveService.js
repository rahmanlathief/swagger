'use strict';


/**
 * get list live / gallery stream
 * get list live / gallery stream
 *
 * acesstoken String 
 * returns SuccessGetStreamContent
 **/
exports.streamdataGET = function(acesstoken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "status" : "ok",
  "msg" : "",
  "data" : [ {
    "category_name" : "Live Streaming",
    "items" : [ {
      "id" : "823",
      "event_name" : "Telkom Click 2020",
      "event_slug" : "telkomclick2020",
      "event_menu_title" : "Telkom Click 2020",
      "event_desc" : "Telkom Click 2020",
      "event_location" : "Telkom Landmark Tower",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_poster_image" : "http://drive.google.com/uc?export=view&id=1FZe3dJrqdsx_nF8Uzj3kgKmDNETd214o",
      "event_poster_image_f" : "http://drive.google.com/uc?export=view&id=1FZe3dJrqdsx_nF8Uzj3kgKmDNETd214o",
      "event_auth" : "token",
      "url_streaming" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/telkomclick2020_final.mp4/Playlist.m3u8",
      "trailer" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/telkomclick2020_final.mp4/Playlist.m3u8",
      "need_login" : "true",
      "cug" : "false"
    } ]
  }, {
    "category_name" : "Perayaan Natal",
    "items" : [ {
      "id" : "804",
      "event_name" : "Perayaan Natal Telkom Group 2018",
      "event_slug" : "perayaan-natal-telkom-group-2018",
      "event_menu_title" : "Perayaan Natal Telkom Group 2018",
      "event_desc" : "Live Streaming",
      "event_location" : "Telkom Landmark Tower",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_poster_image" : "natal-telkom-2018.jpg",
      "event_poster_image_f" : "https://images.useetv.com/natal-telkom-2018.jpg",
      "event_auth" : "free",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/streaming/natal.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/streaming/natal.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "758",
      "event_name" : "Perayaan Natal Telkom Group 2017",
      "event_slug" : "perayaan-natal-telkom-group-2017",
      "event_menu_title" : "Perayaan Natal Telkom Group 2017",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Telkom Landmark Tower",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-natal_telkom_group-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-natal_telkom_group-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/natal2017_360p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/natal2017_360p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    } ]
  }, {
    "category_name" : "Cerita Perut",
    "items" : [ {
      "id" : "226",
      "event_name" : "CERITAPERUT.COM (PART 5)",
      "event_slug" : "ceritaperut-com-part-5",
      "event_menu_title" : "CERITAPERUT.COM (PART 5)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut5-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut5-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT5_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT5_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "225",
      "event_name" : "CERITAPERUT.COM (PART 4)",
      "event_slug" : "ceritaperut-com-part-4",
      "event_menu_title" : "CERITAPERUT.COM (PART 4)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut4-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut4-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT4_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT4_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "224",
      "event_name" : "CERITAPERUT.COM (PART 3)",
      "event_slug" : "ceritaperut-com-part-3",
      "event_menu_title" : "CERITAPERUT.COM (PART 3)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut3-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut3-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT3_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT3_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "223",
      "event_name" : "CERITAPERUT.COM (PART 2)",
      "event_slug" : "ceritaperut-com-part-2",
      "event_menu_title" : "CERITAPERUT.COM (PART 2)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut2-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut2-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT2_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT2_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "222",
      "event_name" : "CERITAPERUT.COM (PART 1)",
      "event_slug" : "ceritaperut-com-part-1",
      "event_menu_title" : "CERITAPERUT.COM (PART 1)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut1-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut1-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT1_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT1_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "221",
      "event_name" : "DMI WAPRES",
      "event_slug" : "dmi-wapres",
      "event_menu_title" : "DMI WAPRES1",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Jakarta",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-dmiwapres-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-dmiwapres-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/WAPRES_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/WAPRES_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    } ]
  }, {
    "category_name" : "Startup Meetup",
    "items" : [ {
      "id" : "187",
      "event_name" : "STARTUP MEETUP V.51",
      "event_slug" : "startup-meetup-v-51",
      "event_menu_title" : "STARTUP MEETUP V.51",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Jakarta",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ok2-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ok2-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/MEETUPV51_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/MEETUPV51_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "185",
      "event_name" : "STARTUP MEETUP BUFFERAPP V.50",
      "event_slug" : "startup-meetup-bufferapp-v-50",
      "event_menu_title" : "STARTUP MEETUP BUFFERAPP V.50",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ok-png-poster.png",
      "event_poster_image_f" : "https://images.useetv.com/event-ok-png-poster.png",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/meetupv50_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/meetupv50_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "183",
      "event_name" : "Startup Meetup Tokopedia V.49",
      "event_slug" : "startup-meetup-tokopedia-v-49",
      "event_menu_title" : "Startup Meetup Tokopedia V.49",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-gukyuk-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-gukyuk-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/tokopedia_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/tokopedia_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "180",
      "event_name" : "Jakarta Digital Valley",
      "event_slug" : "jakarta-digital-valley",
      "event_menu_title" : "Jakarta Digital Valley",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Jakarta",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-event-meet-startup[useetv-landingpage]-poster-poster.png",
      "event_poster_image_f" : "https://images.useetv.com/event-event-meet-startup[useetv-landingpage]-poster-poster.png",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/DigitalValley_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/DigitalValley_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    } ]
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * get detail live streaming / gallery streaming
 * get detail live streaming / gallery streaming
 *
 * acesstoken String 
 * returns SuccessGetDetailStream
 **/
exports.streamdetail804GET = function(acesstoken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "status" : "ok",
  "msg" : "",
  "data" : {
    "detail" : {
      "id" : "804",
      "event_name" : "Perayaan Natal Telkom Group 2018",
      "event_slug" : "perayaan-natal-telkom-group-2018",
      "event_menu_title" : "Perayaan Natal Telkom Group 2018",
      "event_desc" : "Live Streaming",
      "event_location" : "Telkom Landmark Tower",
      "event_start_date" : { },
      "event_end_date" : { },
      "live_start_time" : { },
      "live_end_time" : { },
      "event_poster_image" : "natal-telkom-2018.jpg",
      "event_poster_image_f" : "https://images.useetv.com/natal-telkom-2018.jpg",
      "event_auth" : "free",
      "url_streaming" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/natal.mp4/Playlist.m3u8",
      "trailer" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/natal.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    },
    "related" : [ {
      "id" : "824",
      "event_name" : "Kick Off & Sosialisasi TelkomGroup Awards 2020",
      "event_slug" : "kickofftga2020",
      "event_menu_title" : "Kick Off & Sosialisasi TelkomGroup Awards 2020",
      "event_desc" : "Kick Off Dan Sosialisasi TelkomGroup Awards 2020",
      "event_location" : "Indigo Theater Telkom Corporate University  Center",
      "event_start_date" : { },
      "event_end_date" : { },
      "live_start_time" : { },
      "live_end_time" : { },
      "event_poster_image" : "telkomaward2020.jpg",
      "event_poster_image_f" : "https://images.useetv.com/telkomaward2020.jpg",
      "event_auth" : "free",
      "url_streaming" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/KickoffTGA20.mp4/Playlist.m3u8",
      "trailer" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/KickoffTGA20.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "823",
      "event_name" : "Telkom Click 2020",
      "event_slug" : "telkomclick2020",
      "event_menu_title" : "Telkom Click 2020",
      "event_desc" : "Telkom Click 2020",
      "event_location" : "Telkom Landmark Tower",
      "event_start_date" : { },
      "event_end_date" : { },
      "live_start_time" : { },
      "live_end_time" : { },
      "event_poster_image" : "telkomclick2020.jpg?v=2",
      "event_poster_image_f" : "https://images.useetv.com/telkomclick2020.jpg?v=2",
      "event_auth" : "token",
      "url_streaming" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/telkomclick2020_final.mp4/Playlist.m3u8",
      "trailer" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/telkomclick2020_final.mp4/Playlist.m3u8",
      "need_login" : "true",
      "cug" : "false"
    }, {
      "id" : "804",
      "event_name" : "Perayaan Natal Telkom Group 2018",
      "event_slug" : "perayaan-natal-telkom-group-2018",
      "event_menu_title" : "Perayaan Natal Telkom Group 2018",
      "event_desc" : "Live Streaming",
      "event_location" : "Telkom Landmark Tower",
      "event_start_date" : { },
      "event_end_date" : { },
      "live_start_time" : { },
      "live_end_time" : { },
      "event_poster_image" : "natal-telkom-2018.jpg",
      "event_poster_image_f" : "https://images.useetv.com/natal-telkom-2018.jpg",
      "event_auth" : "free",
      "url_streaming" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/natal.mp4/Playlist.m3u8",
      "trailer" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/natal.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "226",
      "event_name" : "CERITAPERUT.COM (PART 5)",
      "event_slug" : "ceritaperut-com-part-5",
      "event_menu_title" : "CERITAPERUT.COM (PART 5)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut5-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut5-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT5_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT5_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "225",
      "event_name" : "CERITAPERUT.COM (PART 4)",
      "event_slug" : "ceritaperut-com-part-4",
      "event_menu_title" : "CERITAPERUT.COM (PART 4)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut4-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut4-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT4_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT4_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "224",
      "event_name" : "CERITAPERUT.COM (PART 3)",
      "event_slug" : "ceritaperut-com-part-3",
      "event_menu_title" : "CERITAPERUT.COM (PART 3)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut3-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut3-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT3_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT3_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "223",
      "event_name" : "CERITAPERUT.COM (PART 2)",
      "event_slug" : "ceritaperut-com-part-2",
      "event_menu_title" : "CERITAPERUT.COM (PART 2)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut2-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut2-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT2_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT2_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    } ]
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * get detail live streaming / gallery streaming
 * get detail live streaming / gallery streaming
 *
 * acesstoken String 
 * returns SuccessGetDetailStream823
 **/
exports.streamdetail823GET = function(acesstoken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "status" : "ok",
  "msg" : "",
  "data" : {
    "detail" : {
      "id" : "823",
      "event_name" : "Telkom Click 2020",
      "event_slug" : "telkomclick2020",
      "event_menu_title" : "Telkom Click 2020",
      "event_desc" : "Telkom Click 2020",
      "event_location" : "Telkom Landmark Tower",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_poster_image" : "telkomclick2020.jpg?v=2",
      "event_poster_image_f" : "https://images.useetv.com/telkomclick2020.jpg?v=2",
      "event_auth" : "token",
      "url_streaming" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/telkomclick2020_final.mp4/Playlist.m3u8",
      "trailer" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/telkomclick2020_final.mp4/Playlist.m3u8",
      "need_login" : "true",
      "cug" : "false"
    },
    "related" : [ {
      "id" : "824",
      "event_name" : "Kick Off & Sosialisasi TelkomGroup Awards 2020",
      "event_slug" : "kickofftga2020",
      "event_menu_title" : "Kick Off & Sosialisasi TelkomGroup Awards 2020",
      "event_desc" : "Kick Off Dan Sosialisasi TelkomGroup Awards 2020",
      "event_location" : "Indigo Theater Telkom Corporate University  Center",
      "event_start_date" : { },
      "event_end_date" : { },
      "live_start_time" : { },
      "live_end_time" : { },
      "event_poster_image" : "telkomaward2020.jpg",
      "event_poster_image_f" : "https://images.useetv.com/telkomaward2020.jpg",
      "event_auth" : "free",
      "url_streaming" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/KickoffTGA20.mp4/Playlist.m3u8",
      "trailer" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/KickoffTGA20.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "823",
      "event_name" : "Telkom Click 2020",
      "event_slug" : "telkomclick2020",
      "event_menu_title" : "Telkom Click 2020",
      "event_desc" : "Telkom Click 2020",
      "event_location" : "Telkom Landmark Tower",
      "event_start_date" : { },
      "event_end_date" : { },
      "live_start_time" : { },
      "live_end_time" : { },
      "event_poster_image" : "telkomclick2020.jpg?v=2",
      "event_poster_image_f" : "https://images.useetv.com/telkomclick2020.jpg?v=2",
      "event_auth" : "token",
      "url_streaming" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/telkomclick2020_final.mp4/Playlist.m3u8",
      "trailer" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/telkomclick2020_final.mp4/Playlist.m3u8",
      "need_login" : "true",
      "cug" : "false"
    }, {
      "id" : "804",
      "event_name" : "Perayaan Natal Telkom Group 2018",
      "event_slug" : "perayaan-natal-telkom-group-2018",
      "event_menu_title" : "Perayaan Natal Telkom Group 2018",
      "event_desc" : "Live Streaming",
      "event_location" : "Telkom Landmark Tower",
      "event_start_date" : { },
      "event_end_date" : { },
      "live_start_time" : { },
      "live_end_time" : { },
      "event_poster_image" : "natal-telkom-2018.jpg",
      "event_poster_image_f" : "https://images.useetv.com/natal-telkom-2018.jpg",
      "event_auth" : "free",
      "url_streaming" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/natal.mp4/Playlist.m3u8",
      "trailer" : "https://vod2.useetv.com/vod/mp4:vod/vod/streaming/natal.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "226",
      "event_name" : "CERITAPERUT.COM (PART 5)",
      "event_slug" : "ceritaperut-com-part-5",
      "event_menu_title" : "CERITAPERUT.COM (PART 5)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut5-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut5-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT5_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT5_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "225",
      "event_name" : "CERITAPERUT.COM (PART 4)",
      "event_slug" : "ceritaperut-com-part-4",
      "event_menu_title" : "CERITAPERUT.COM (PART 4)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut4-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut4-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT4_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT4_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "224",
      "event_name" : "CERITAPERUT.COM (PART 3)",
      "event_slug" : "ceritaperut-com-part-3",
      "event_menu_title" : "CERITAPERUT.COM (PART 3)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut3-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut3-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT3_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT3_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    }, {
      "id" : "223",
      "event_name" : "CERITAPERUT.COM (PART 2)",
      "event_slug" : "ceritaperut-com-part-2",
      "event_menu_title" : "CERITAPERUT.COM (PART 2)",
      "event_subtitle" : "",
      "event_desc" : "",
      "event_location" : "Pasar Santa",
      "event_start_date" : { },
      "event_end_date" : { },
      "event_thumbs" : "",
      "event_poster_image" : "event-ceritaperut2-poster.jpg",
      "event_poster_image_f" : "https://images.useetv.com/event-ceritaperut2-poster.jpg",
      "event_auth" : "free",
      "event_extra_content" : "",
      "extra_video" : "",
      "url_streaming" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT2_240p.mp4/Playlist.m3u8",
      "trailer" : "http://118.97.96.2/vod/mp4:vod/vod/live_streaming/CERITAPERUT2_240p.mp4/Playlist.m3u8",
      "need_login" : "false",
      "cug" : "false"
    } ]
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

