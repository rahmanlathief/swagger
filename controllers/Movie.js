'use strict';

var utils = require('../utils/writer.js');
var Movie = require('../service/MovieService');

module.exports.moviesGET = function moviesGET (req, res, next, acesstoken) {
  Movie.moviesGET(acesstoken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.moviesdetail1GET = function moviesdetail1GET (req, res, next, acesstoken, page, size) {
  Movie.moviesdetail1GET(acesstoken, page, size)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.moviesdetail2GET = function moviesdetail2GET (req, res, next, acesstoken, page, size) {
  Movie.moviesdetail2GET(acesstoken, page, size)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.moviesdetail3GET = function moviesdetail3GET (req, res, next, acesstoken, page, size) {
  Movie.moviesdetail3GET(acesstoken, page, size)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
