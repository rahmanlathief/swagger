'use strict';

var path = require('path');
var http = require('http');

var oas3Tools = require('oas3-tools');
var serverPort = 8081;

// swaggerRouter configuration
var options = {
    controllers: path.join(__dirname, './controllers')
};

var expressAppConfig = oas3Tools.expressAppConfig(path.join(__dirname, 'api/openapi.yaml'), options);
expressAppConfig.addValidator();
var app = expressAppConfig.getApp();

//Initialize the Swagger middleware
http.createServer(app)
    .listen(serverPort, function (portName) {
        console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
        console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
})
    .on('error', function (err) {

       if (err.code === 'EADDRINUSE'){
        serverPort++;
        http.createServer(app).listen(serverPort);
        console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
        console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
       }
        
    })

// HTTPServer
// .listen(port, function() {
//   console.log('we have created' + listenerCounter + ' http server listener');
//   listenerCounter++;
//   console.log('HTTP listening: ' + port);
// })
// .on('error', function (err) {
//   if (err.code === 'EADDRINUSE') {
//     port++;
//     console.log('Adresss in use, retrying on port ' + port);
//     setTimeout(function() {
//       HTTPServer.listen(port);
//     }, 250);
//   }
// })
