'use strict';

var utils = require('../utils/writer.js');
var Live = require('../service/LiveService');

module.exports.streamdataGET = function streamdataGET (req, res, next, acesstoken) {
  Live.streamdataGET(acesstoken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.streamdetail804GET = function streamdetail804GET (req, res, next, acesstoken) {
  Live.streamdetail804GET(acesstoken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.streamdetail823GET = function streamdetail823GET (req, res, next, acesstoken) {
  Live.streamdetail823GET(acesstoken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
