'use strict';

var utils = require('../utils/writer.js');
var AccessToken = require('../service/AccessTokenService');

module.exports.accessTokenPOST = function accessTokenPOST (req, res, next, acesstoken) {
  AccessToken.accessTokenPOST(acesstoken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
