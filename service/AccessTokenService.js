'use strict';


/**
 * get access token
 * geta access token
 *
 * acesstoken String 
 * returns accessToken
 **/
exports.accessTokenPOST = function(acesstoken) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "resultCode\"" : 1,
  "resultMessage" : "success",
  "access_token" : "acc66ac32cbd13c55ab8ebfbdd719beb",
  "android_version" : "5.4.0",
  "ios_version" : "2.0.1",
  "stb_version" : "1.0.0.0"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

