'use strict';

var utils = require('../utils/writer.js');
var HomeContent = require('../service/HomeContentService');

module.exports.homemobileDataGET = function homemobileDataGET (req, res, next, acesstoken) {
  HomeContent.homemobileDataGET(acesstoken)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
